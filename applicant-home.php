<?php include 'header.php'; ?>

    <header class="page__header page__header--candidate-home section-turquoise">
        <div class="grid">
            <div class="col-1-1">
                <h1 class="page__title">Home - Welcome [applicant’s name]</h1>
                <span class="last-login">Last Login: 21:48 | 28/10/14</span>
            </div>
        </div>
    </header>

    <div class="page__body page__body--candidate-home section">
        <div class="page__feedback">
            <div class="grid">
                <div class="col-1-1 blue-box">
                    <i class="ficon ficon-ok-circled color-pink"></i>
                    <span>[Applicant name] thank you for subscribing to softgarden. You can update your profile <a href="applicant-profile.php">here</a>.</span>
                </div>
            </div>
        </div>

        <div class="page__content">
            <div class="grid">
                <div class="col-1-1">
                    <section class="applicant-home__info">
                        <header class="section__header">
                            <h2 class="section__title">Interesting Information</h2>
                        </header>
                        <div class="section__body">
                            <div class="grid">
                                <div class="col-1-3 col-1-3 left-pad0">
                                    <span class="extra-large">36 Views</span>
                                    <p>Number of times your profile has appeared in company searches</p>
                                </div>
                                <div class="col-1-3 col-1-3">
                                    <span class="extra-large">26 New jobs</span>
                                    <p>New jobs that have been posted since your last login<br /><a href="applicant-jobs.php">View</a></p>
                                </div>
                                <div class="col-1-3 col-1-3">
                                    <span class="extra-large">3 Applications</span>
                                    <p>Number of your applications<br /><a href="applicant-applications.php">View</a></p>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="applicant-home__job-proposals">
                        <header class="section__header">
                            <h2 class="section__title">Jobs that may interest you</h2>
                        </header>
                        <div class="section__body">

                            <?php include 'job-list.php'; ?>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
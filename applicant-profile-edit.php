<?php include 'header.php'; ?>

    <header class="page__header page__header--profile-edit section-turquoise">
        <div class="grid">
            <div class="col-1-1">
                <h1 class="page__title">Your Profile - Edit</h1>
            </div>
        </div>
    </header>
    <div class="page__body page__body--profile-edit section">
        <div class="grid">
            <div class="col-1-1">
                <section class="profile__edit">
                    <?php include 'application-form.php'; ?>
                 </section>
             </div>
         </div>
     </div>

<?php include 'footer.php'; ?>
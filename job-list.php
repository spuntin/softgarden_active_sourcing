<table class="job-list__table bottom-marg30">
    <thead>
        <tr>
            <th><a href="">Company</a></th>
            <th><a href="">Job</a></th>
            <th><a href="">Category</a></th>
            <th><a href="">Location</a></th>
            <th><a href="">Date <i class="ficon ficon-down-open"></i></a></th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><img src="img/logo-jobs-available-vitra.png" alt="Vitra" width="74" height="25"></td>
            <td>Senior Marketing Manager m/w - E-Recruiting Soft...</td>
            <td>Marketing/Sales</td>
            <td>Berlin, DE</td>
            <td>04.09.14</td>
        </tr>
        <tr>
            <td><img src="img/logo-jobs-available-brainlab.png" alt="BRAINLAB" width="75" height="10"></td>
            <td>Growth Marketing Manager</td>
            <td>Marketing/Sales</td>
            <td>Mönchengladb...</td>
            <td>04.09.14</td>
        </tr>
    </tbody>
</table>
<button class="btn-turquoise float-right">More</button>
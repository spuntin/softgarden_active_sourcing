<div class="section__body section__body--profile-form">
    <div class="grid">
        <div class="import__profile clearfix">
            <div class="import__profile--linkedin float-left">
                <a href=""><img src="img/btn-linkedin.png" alt="Import from LinkedIn" width="242" height="35"></a>
            </div>
            <div class="import__profile--divider float-left">
                or
            </div>
            <div class="import__profile--xing float-left">
                <a href=""><img src="img/btn-xing.png" alt="Import from XING" width="242" height="35"></a>
            </div>
            <span class="import__profile-caption">Or fill out this form:</span>
        </div>


        <!-- ************************************************************************

            Form copied from Add Application in Just Hire 

        ***************************************************************************** -->

        <form class="personal__form" id="" method="post" action="" enctype="multipart/form-data" accept-charset="UTF-8">
            <section id="application-personal-data" class="application-personal-data application-edit-section">
                <header>
                    <a class="toggle-details-link">
                        <a class="toggle-details-link" href="#" id="id272">
                            <span class="show-details">More Details <i class="ficon ficon-down-open"></i></span>
                            <span class="hide-details">Hide Details <i class="ficon ficon-up-open"></i></span>
                        </a>
                    </a>
                    <h2>Personal Details</h2>
                </header>
                <section id="application-personal-base-data" class="application-personal-base-data">
                    <div class="photo-upload" id="id265">
                        <div class="drag-and-drop-upload-panel">
                            <div class="drag-and-drop-upload-form" id="id271">
                                <div class="drag-and-drop-upload-container" id="id274">
                                    <div class="drag-and-drop-area">
                                        <div class="upload-stage">
                                            <div class="upload-no-data" id="id275">
                                                <i class="ficon ficon-upload"></i>
                                                <div class="upload-hint-dd upload-hint">
                                                    Drag and drop here or <a class="upload-link" href="#" id="id266">upload</a> image
                                                </div>
                                                <div class="upload-hint-fb upload-hint">
                                                    <div class="upload-hint-fb-std">
                                                        <a class="upload-link" href="#" id="id267">Upload</a> now
                                                    </div>
                                                    <div class="upload-hint-fb-ie">
                                                        <a class="upload-link" href="#" id="id268">Upload</a> now
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="application-base-information clearfix">
                        <div class="input-title-container clearfix">
                            <div class="input-group-salutation control-group">
                                <label class="control-label">Salutation</label>
                                <div class="controls">
                                    <select class="input-salutation input-block-level" name="body:fillOfflineApplicationPanel:baseDataPanel:salutationGroup:salutationGroup_body:salutationChoice">
                                        <option value="" disabled selected>Please Choose</option>
                                        <option value="0">Mr</option>
                                        <option value="1">Mrs</option>
                                    </select>
                                </div>
                            </div>
                            <div class="input-group-title control-group">
                                <label class="control-label">Title</label>
                                <div class="controls">
                                    <select class="input-salutation input-block-level" name="body:fillOfflineApplicationPanel:baseDataPanel:titleGroup:salutationGroup_body:titleChoice">
                                        <option value="" disabled selected>Please Choose</option>
                                        <option value="0">Title1</option>
                                        <option value="1">Title2</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="input-name-container clearfix">
                            <div class="input-group-firstname control-group">
                                <label class="control-label">First name</label>
                                <div class="controls">
                                    <input type="text" class="input-firstname input-block-level" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:firstNameGroup:firstNameGroup_body:firstname"/>
                                </div>
                            </div>
                            <div class=" input-group-lastname control-group">
                                <label class="control-label">Last name</label>
                                <div class="controls">
                                    <input type="text" class="input-lastname input-block-level" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:lastNameGroup:lastNameGroup_body:lastname"/>
                                </div>
                            </div>
                        </div>
                        <div class=" input-group-email control-group">
                            <label class="control-label">Email Address</label>
                            <div class="controls">
                                <input type="text" class="input-email input-block-level" value="askjfsh@hotmail.com" name="body:fillOfflineApplicationPanel:baseDataPanel:emailGroup:emailGroup_body:email" disabled="disabled"/>
                            </div>
                        </div>
                        <div class=" input-phone-group control-group">
                            <label class="control-label">Phone</label>
                            <div class="controls">
                                <input class="input-block-level" type="text" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:detailPanel:phoneGroup:phoneGroup_body:phone"/>
                            </div>
                        </div>
                    </div>
                </section>
                <section id="application-personal-details-data" class="application-personal-details-data clearfix">
                    <div class=" control-group input-street-group">
                        <label class="control-label">Street, Nr.</label>
                        <div class="controls">
                            <input class="input-block-level" type="text" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:detailPanel:streetGroup:streetGroup_body:street"/>
                        </div>
                    </div>
                    <div class=" input-city-group control-group">
                        <label class="control-label">City</label>
                        <div class="controls">
                            <input class="input-block-level" type="text" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:detailPanel:cityGroup:cityGroup_body:city"/>
                        </div>
                    </div>
                    <div class=" input-zip-group control-group">
                        <label class="control-label">ZIP code</label>
                        <div class="controls">
                            <input class="input-block-level" type="text" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:detailPanel:zipCodeGroup:zipCodeGroup_body:zip"/>
                        </div>
                    </div>
                    <div class="input-country-group  control-group">
                        <label class="control-label">Country</label>
                        <div class="controls">
                            <input class="input-block-level" type="text" value="" name="body:fillOfflineApplicationPanel:baseDataPanel:detailPanel:countryGroup:countryGroup_body:country"/>
                        </div>
                    </div>
                </section>
            </section>
            <section id="application-profile-data" class="application-profile-data application-edit-section clearfix">
                <header class="profile-panel-title"><h2>Resume</h2></header>
                <div class="application-profile-manual-fill">
                    <div class="manual-application-data-container" id="id27b">      
                        <section id="application-profile-experience" class="application-manual-profile-section">
                            <header class="profile-panel-title">Experience</header>
                            <div class="profile-panel-container" id="id19d">
                                <a class="profile-panel-add-link rpg-add-link" id="id128" href="javascript:;" style="display: none;"><i class="ficon ficon-plus-circled"></i> Add a position</a>
                                <div class="add-experience-panel rpg-new" id="id1ae">
                                    <div class="edit-exp-panel">
                                        <div class="two-columns clearfix">
                                            <div class="left-column">
                                                <div class=" cg-e-pos control-group">
                                                    <label class="control-label">Position</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:positionGroup:positionGroup_body:position">
                                                    </div>
                                                </div>
                                                <div class=" control-group cg-e-c">
                                                    <label class="control-label">Company</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:companyGroup:companyGroup_body:company">
                                                    </div>
                                                </div>
                                                <div class=" control-group cg-e-web">
                                                    <label class="control-label">Company homepage</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:homepageGroup:homepageGroup_body:homepage">
                                                    </div>
                                                </div>
                                                <div class=" control-group cg-e-from">
                                                    <label class="control-label">from</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:fromGroup:fromGroup_body:from" id="id1a8">
                                                    </div>
                                                </div>
                                                <div class=" cg-e-to control-group" id="id1af">
                                                    <label class="control-label">to</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:toGroup:toGroup_body:to" id="id1a9">
                                                    </div>
                                                </div>
                                                <div class="cg-e-to-today control-group">
                                                    <label class="checkbox" id="id1aa-w-lbl" for="id1aa"><input type="checkbox" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:today" id="id1aa"> until today</label>
                                                </div>
                                            </div>
                                            <div class="right-column">
                                                <div class=" cg-e-lvl control-group">
                                                    <label class="control-label">Career level</label>
                                                    <div class="controls">
                                                        <div>
                                                            <select name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:careerLevelGroup:careerLevelGroup_body:careerLevel:catalogChoice">
                                                                <option selected="selected" value="">Choose One</option>
                                                                <option value="STUDENT_INTERN">Student/Intern</option>
                                                                <option value="ENTRY_LEVEL">Entry Level</option>
                                                                <option value="PROFESSIONAL_EXPERIENCED">Professional/Experienced</option>
                                                                <option value="MANAGER_SUPERVISOR">Manager (Manager/Supervisor)</option>
                                                                <option value="EXECUTIVE">Executive (VP, SVP, etc.)</option>
                                                                <option value="SENIOR_EXECUTIVE">Senior Executive (CEO, CFO, President)</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" cg-e-c-size control-group">
                                                    <label class="control-label">Company size</label>
                                                    <div class="controls">
                                                        <div>
                                                            <select name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:companySizeGroup:companySizeGroup_body:companySize:catalogChoice">
                                                                <option selected="selected" value="">Choose One</option>
                                                                <option value="0">Not specified</option>
                                                                <option value="1">Just me</option>
                                                                <option value="1-10">1-10 employees</option>
                                                                <option value="11-50">11-50 employees</option>
                                                                <option value="51-200">51-200 employees</option>
                                                                <option value="201-500">201-500 employees</option>
                                                                <option value="501-1000">501-1,000 employees</option>
                                                                <option value="1001-5000">1,001-5,000 employees</option>
                                                                <option value="5001-10000">5,001-10,000 employees</option>
                                                                <option value="10001+">10,001 or more employees</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class=" cg-e-c-indstry control-group">
                                                    <label class="control-label">Industry</label>
                                                    <div class="controls">
                                                        <div>
                                                            <select name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:industryGroup:industryGroup_body:industry:catalogChoice">
                                                                <option selected="selected" value="">Choose One</option>
                                                                <option value="ACADEMIA">Academia</option>
                                                                <option value="ACCOUNTING">Accounting</option>
                                                                <option value="AEROSPACE">Aerospace</option>
                                                                <option value="AGRICULTURE">Agriculture</option>
                                                                <option value="AIRLINES">Airlines</option>
                                                                <option value="ALTERNATIVE_MEDICINE">Alternative Medicine</option>
                                                                <option value="APPAREL_AND_FASHION">Apparel &amp; Fashion</option>
                                                                <option value="ARCHITECTURE_AND_PLANNING">Architecture &amp; Planning</option>
                                                                <option value="ARTS_AND_CRAFTS">Arts &amp; Crafts</option>
                                                                <option value="AUTOMOTIVE">Automotive</option>
                                                                <option value="BANKING">Banking</option>
                                                                <option value="BIOTECHNOLOGY">Biotechnology</option>
                                                                <option value="BROADCAST_MEDIA">Broadcast Media</option>
                                                                <option value="BUILDING_MATERIALS">Building Materials</option>
                                                                <option value="BUSINESS_SUPPLIES_AND_EQUIPMENT">Business Supplies &amp; Equipment</option>
                                                                <option value="CHEMICALS">Chemicals</option>
                                                                <option value="CIVIC_AND_SOCIAL_ORGANIZATIONS">Civic &amp; Social Organizations</option>
                                                                <option value="CIVIL_ENGINEERING">Civil Engineering</option>
                                                                <option value="CIVIL_SERVICE">Civil Service</option>
                                                                <option value="COMPOSITES">Composites</option>
                                                                <option value="COMPUTER_AND_NETWORK_SECURITY">Computer &amp; Network Security</option>
                                                                <option value="COMPUTER_GAMES">Computer Games</option>
                                                                <option value="COMPUTER_HARDWARE">Computer Hardware</option>
                                                                <option value="COMPUTER_NETWORKING">Computer Networking</option>
                                                                <option value="COMPUTER_SOFTWARE">Computer Software</option>
                                                                <option value="CONSTRUCTION">Construction</option>
                                                                <option value="CONSULTING">Consulting</option>
                                                                <option value="CONSUMER_ELECTRONICS">Consumer Electronics</option>
                                                                <option value="CONSUMER_GOODS">Consumer Goods</option>
                                                                <option value="CONSUMER_SERVICES">Consumer Services</option>
                                                                <option value="COSMETICS">Cosmetics</option>
                                                                <option value="DAYCARE">Daycare</option>
                                                                <option value="DEFENSE_MILITARY">Defense/Military</option>
                                                                <option value="DESIGN">Design</option>
                                                                <option value="ELEARNING">E-learning</option>
                                                                <option value="EDUCATION">Education</option>
                                                                <option value="ELECTRICAL_ENGINEERING">Electrical Engineering</option>
                                                                <option value="ENERGY">Energy</option>
                                                                <option value="ENTERTAINMENT">Entertainment</option>
                                                                <option value="ENVIRONMENTAL_SERVICES">Environmental Services</option>
                                                                <option value="EVENTS_SERVICES">Events Services</option>
                                                                <option value="FACILITIES_SERVICES">Facilities Services</option>
                                                                <option value="FACILITY_MANAGEMENT">Facility Management</option>
                                                                <option value="FINANCIAL_SERVICES">Financial Services</option>
                                                                <option value="FISHERY">Fishery</option>
                                                                <option value="FOOD">Food</option>
                                                                <option value="FUNDRAISING">Fundraising</option>
                                                                <option value="FURNITURE">Furniture</option>
                                                                <option value="GARDENING_LANDSCAPING">Gardening/Landscaping</option>
                                                                <option value="GEOLOGY">Geology</option>
                                                                <option value="GLASS_AND_CERAMICS">Glass &amp; Ceramics</option>
                                                                <option value="GRAPHIC_DESIGN">Graphic Design</option>
                                                                <option value="HEALTH_AND_FITNESS">Health &amp; Fitness</option>
                                                                <option value="HOSPITALITY">Hospitality</option>
                                                                <option value="HUMAN_RESOURCES">Human Resources</option>
                                                                <option value="IMPORT_AND_EXPORT">Import &amp; Export</option>
                                                                <option value="INDUSTRIAL_AUTOMATION">Industrial Automation</option>
                                                                <option value="INFORMATION_SERVICES">Information Services</option>
                                                                <option value="INFORMATION_TECHNOLOGY_AND_SERVICES">Information Technology &amp; Services</option>
                                                                <option value="INSURANCE">Insurance</option>
                                                                <option value="INTERNATIONAL_AFFAIRS">International Affairs</option>
                                                                <option value="INTERNATIONAL_TRADE_AND_DEVELOPMENT">International Trade &amp; Development</option>
                                                                <option value="INTERNET">Internet</option>
                                                                <option value="INVESTMENT_BANKING">Investment Banking</option>
                                                                <option value="JOURNALISM">Journalism</option>
                                                                <option value="LEGAL_SERVICES">Legal Services</option>
                                                                <option value="LEISURE_TRAVEL_AND_TOURISM">Leisure, Travel &amp; Tourism</option>
                                                                <option value="LIBRARIES">Libraries</option>
                                                                <option value="LOGISTICS_AND_SUPPLY_CHAIN">Logistics &amp; Supply Chain</option>
                                                                <option value="LUXURY_GOODS_AND_JEWELRY">Luxury Goods &amp; Jewelry</option>
                                                                <option value="MACHINERY">Machinery</option>
                                                                <option value="MANAGEMENT_CONSULTING">Management Consulting</option>
                                                                <option value="MARITIME">Maritime</option>
                                                                <option value="MARKET_RESEARCH">Market Research</option>
                                                                <option value="MARKETING_AND_ADVERTISING">Marketing &amp; Advertising</option>
                                                                <option value="MECHANICAL_INDUSTRIAL_ENGINEERING">Mechanical/Industrial Engineering</option>
                                                                <option value="MEDIA_PRODUCTION">Media Production</option>
                                                                <option value="MEDICAL_DEVICES">Medical Devices</option>
                                                                <option value="MEDICAL_SERVICES">Medical Services</option>
                                                                <option value="MEDICINAL_PRODUCTS">Medicinal Products</option>
                                                                <option value="METAL_METALWORKING">Metal/Metalworking</option>
                                                                <option value="METROLOGY_CONTROL_ENGINEERING">Metrology/Control Engineering</option>
                                                                <option value="MINING_AND_METALS">Mining &amp; Metals</option>
                                                                <option value="MOTION_PICTURES">Motion Pictures</option>
                                                                <option value="MUSEUMS_AND_CULTURAL_INSTITUTIONS">Museums &amp; Cultural Institutions</option>
                                                                <option value="MUSIC">Music</option>
                                                                <option value="NANOTECHNOLOGY">Nanotechnology</option>
                                                                <option value="NON_PROFIT_ORGANIZATION">Non-Profit Organization</option>
                                                                <option value="NURSING_AND_PERSONAL_CARE">Nursing &amp; Personal Care</option>
                                                                <option value="OIL_AND_ENERGY">Oil &amp; Energy</option>
                                                                <option value="ONLINE_MEDIA">Online Media</option>
                                                                <option value="OTHERS">Others</option>
                                                                <option value="OUTSOURCING_OFFSHORING">Outsourcing/Offshoring</option>
                                                                <option value="PACKAGING_AND_CONTAINERS">Packaging &amp; Containers</option>
                                                                <option value="PAPER_AND_FOREST_PRODUCTS">Paper &amp; Forest Products</option>
                                                                <option value="PHARMACEUTICALS">Pharmaceuticals</option>
                                                                <option value="PHOTOGRAPHY">Photography</option>
                                                                <option value="PLASTICS">Plastics</option>
                                                                <option value="POLITICS">Politics</option>
                                                                <option value="PRINT_MEDIA">Print Media</option>
                                                                <option value="PRINTING">Printing</option>
                                                                <option value="PROCESS_MANAGEMENT">Process Management</option>
                                                                <option value="PROFESSIONAL_TRAINING_AND_COACHING">Professional Training &amp; Coaching</option>
                                                                <option value="PSYCHOLOGY_PSYCHOTHERAPY">Psychology/Psychotherapy</option>
                                                                <option value="PUBLIC_HEALTH">Public Health</option>
                                                                <option value="PUBLIC_RELATIONS_AND_COMMUNICATIONS">Public Relations &amp; Communications</option>
                                                                <option value="PUBLISHING">Publishing</option>
                                                                <option value="RAILROAD">Railroad</option>
                                                                <option value="REAL_ESTATE">Real Estate</option>
                                                                <option value="RECREATIONAL_FACILITIES_AND_SERVICES">Recreational Facilities &amp; Services</option>
                                                                <option value="RECYCLING_AND_WASTE_MANAGEMENT">Recycling &amp; Waste Management</option>
                                                                <option value="RENEWABLES_AND_ENVIRONMENT">Renewables &amp; Environment</option>
                                                                <option value="RESEARCH">Research</option>
                                                                <option value="RESTAURANTS_AND_FOOD_SERVICE">Restaurants &amp; Food Service</option>
                                                                <option value="RETAIL">Retail</option>
                                                                <option value="SECURITY_AND_INVESTIGATIONS">Security &amp; Investigations</option>
                                                                <option value="SEMICONDUCTORS">Semiconductors</option>
                                                                <option value="SHIPBUILDING">Shipbuilding</option>
                                                                <option value="SPORTS">Sports</option>
                                                                <option value="STAFFING_AND_RECRUITING">Staffing &amp; Recruiting</option>
                                                                <option value="TAX_ACCOUNTANCY_AUDITING">Tax accountancy/Auditing</option>
                                                                <option value="TELECOMMUNICATION">Telecommunication</option>
                                                                <option value="TEXTILES">Textiles</option>
                                                                <option value="THEATER_STAGE_CINEMA">Theater/Stage/Cinema</option>
                                                                <option value="TIMBER">Timber</option>
                                                                <option value="TRAFFIC_ENGINEERING">Traffic Engineering</option>
                                                                <option value="TRANSLATION_AND_LOCALIZATION">Translation &amp; Localization</option>
                                                                <option value="TRANSPORT">Transport</option>
                                                                <option value="VENTURE_CAPITAL_AND_PRIVATE_EQUITY">Venture Capital &amp; Private Equity</option>
                                                                <option value="VETERINARY">Veterinary</option>
                                                                <option value="WELFARE_AND_COMMUNITY_HEALTH">Welfare &amp; Community Health</option>
                                                                <option value="WHOLESALE">Wholesale</option>
                                                                <option value="WINE_AND_SPIRITS">Wine &amp; Spirits</option>
                                                                <option value="WRITING_AND_EDITING">Writing &amp; Editing</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class=" control-group cg-e-c-desc">
                                            <label class="control-label">Describe your position</label>
                                            <div class="controls">
                                                <textarea name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:experiencePanel:experienceContainer:addPanel:experienceForm:descriptionGroup:descriptionGroup_body:description"></textarea>
                                            </div>
                                        </div>
                                        <div class="cg-e-actions">
                                            <button class="btn btn-save btn-turquoise float-right">Save</button>
                                            <button class="btn btn-cancel btn-grey float-left">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="application-profile-education" class="application-profile-education application-manual-profile-section">
                            <header class="profile-panel-title">Education</header>
                            <div class="profile-panel-container" id="id293">
                                <div class="education-new rpg-new" id="id2a2">
                                    <div class="profile-panel-add-panel">
                                        <div class="two-columns">
                                            <div class="left-column">
                                                <div class=" control-group edu-grp-fos">
                                                    <label class="control-label">Field of study</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:educationPanel:educationContainer:addPanel:educationForm:fieldGroup:fieldGroup_body:field"/>
                                                    </div>
                                                </div>
                                                <div class="edu-grp-uni  control-group">
                                                    <label class="control-label">University</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:educationPanel:educationContainer:addPanel:educationForm:universityGroup:universityGroup_body:university"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="right-column">
                                                <div class="edu-grp-dgr  control-group">
                                                    <label class="control-label">Degree</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:educationPanel:educationContainer:addPanel:educationForm:degreeGroup:degreeGroup_body:degree"></input>
                                                    </div>
                                                </div>
                                                <div class=" edu-grp-from control-group">
                                                    <label class="control-label">from</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:educationPanel:educationContainer:addPanel:educationForm:fromGroup:fromGroup_body:from" id="id29d"/>
                                                    </div>
                                                </div>
                                                <div class=" control-group edu-grp-to">
                                                    <label class="control-label">to</label>
                                                    <div class="controls">
                                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:educationPanel:educationContainer:addPanel:educationForm:toGroup:toGroup_body:to" id="id29e"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="edu-grp-sp  control-group">
                                            <label class="control-label">Specialized subjects</label>
                                            <div class="controls">
                                                <textarea name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:educationPanel:educationContainer:addPanel:educationForm:specializationGroup:specializationGroup_body:specialization"></textarea>
                                            </div>
                                        </div>
                                        <div class="edu-grp-actions">
                                            <button class="btn btn-save btn-turquoise float-right">Save</button>
                                            <button class="btn btn-cancel btn-grey float-left">Cancel</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="application-profile-language" class="application-profile-language application-manual-profile-section">
                            <header>Languages</header>
                            <div class="add-language-container language-entry">
                                <div class="new-language language-input-grp">
                                    <div class="language">
                                        <input type="text" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:languagePanel:addLanguageForm:newLanguage:language" id="id281" autocomplete="off" placeholder="Language" class="js-placeholder"></input>
                                    </div>
                                    <div class="skill">
                                        <select name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:languagePanel:addLanguageForm:newLanguage:skill" placeholder="Proficiency" class="js-placeholder">
                                            <option value="1.0">Basic knowledge</option>
                                            <option value="2.0">Good knowledge</option>
                                            <option value="3.0">Fluent</option>
                                            <option value="4.0">First language</option>
                                        </select>
                                    </div>
                                </div>
                                <a class="add-language-link" id="id280" href="javascript:;"><i class="ficon ficon-plus-circled"></i></a>
                            </div>
                        </section>
                        <section id="application-profile-additional" class="application-profile-additional application-manual-profile-section">
                            <header>
                                <a class="toggle-additional-info-link toggle-details-link" href="#" id="id291">Additional information <i class="ficon ficon-down-open"></i></a>
                            </header>
                            <div id="id25a">
                                <div id="application-coverletter" class="application-coverletter application-additional-content control-group clearfix">
                                    <div class="control-label">Cover letter</div>
                                    <div class="controls" id="id297">
                                        <a href="javascript:;" class="sneaky-link" id="id289">Textual cover letter</a> or
                                        <div class="attachment-list-panel">
                                            <div class="attachment-lst">
                                                <div class="attachment-lst-itm-cnt">
                                                    <div class="attachment-lst-itm">
                                                    </div>
                                                </div>
                                                <div class="new-attachment not-yet-uploaded">
                                                    <span class="filename"></span>
                                                </div>
                                            </div>
                                            <div class="upload-attachment-container" id="id286">
                                                <div class="progress-upload-panel">
                                                    <div id="id298" style="display:none"></div>
                                                    <div id="id285">
                                                        <div class="attachment-lst progress-bar-container">
                                                            <div class="attachment-item">
                                                                <div class="inline-progress-bar-border" style="display: none;" id="id284">
                                                                    <div class="inline-progress-bar-container">
                                                                        <div class="progress-bar">
                                                                            <div class=""></div>
                                                                        </div>
                                                                        <div class="attachment">
                                                                            <div class="attachment-link" style="background-color: transparent;">
                                                                                <div class="filename"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wupb-uploadStatus" style="display: none;" id="id283"></div>
                                                        <div class="upload-form">
                                                            <input type="file" class="hidden-upload-field" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:additionalInformation:toggleContainer:uploadCoverLetter:uploadPanel:form:fileUploadField" id="id282"/>
                                                            <input class="upload-submit-button" type="button" value="Hochladen" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:additionalInformation:toggleContainer:uploadCoverLetter:uploadPanel:form:ajaxSubmit" id="id287"/>
                                                        </div>
                                                    </div>
                                                    <div class="feigned-upload-button-aligning-wrapper">
                                                        <a href="#" class="btn btn-upload" id="id288"><i class="ficon ficon-upload"></i> Upload an attachment <span class="max-file-size">(<span class="size">5M</span> max)</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <textarea id="id299" style="display:none"></textarea>
                                    </div>
                                </div>
                                <div id="application-attachments" class="application-attachments application-attachments-section application-additional-content control-group">
                                    <div class="control-label">Additional attachments</div>
                                    <div class="controls" id="id29a">
                                        <div class="attachment-list-panel">
                                            <div class="attachment-lst" id="id29b">
                                                <div class="new-attachment not-yet-uploaded">
                                                    <span class="filename"></span>
                                                </div>
                                            </div>
                                            <div class="upload-attachment-container" id="id28e">
                                                <div class="progress-upload-panel">
                                                    <div id="id29c" style="display:none"></div>
                                                    <div id="id28d">
                                                        <div class="attachment-lst progress-bar-container">
                                                            <div class="attachment-item">
                                                                <div class="inline-progress-bar-border" style="display: none;" id="id28c">
                                                                    <div class="inline-progress-bar-container">
                                                                        <div class="progress-bar">
                                                                            <div class=""></div>
                                                                        </div>
                                                                        <div class="attachment">
                                                                            <div class="attachment-link" style="background-color: transparent;">
                                                                                <div class="filename"></div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wupb-uploadStatus" style="display: none;" id="id28b"></div>
                                                        <div class="upload-form">
                                                            <input type="file" class="hidden-upload-field" value="" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:additionalInformation:toggleContainer:uploadList:uploadPanel:form:fileUploadField" id="id28a"/>
                                                            <input class="upload-submit-button" type="button" value="Hochladen" name="body:fillOfflineApplicationPanel:profileDataPanel:group:fillManuallyPanel:additionalInformation:toggleContainer:uploadList:uploadPanel:form:ajaxSubmit" id="id28f"/>
                                                        </div>
                                                    </div>
                                                    <div class="feigned-upload-button-aligning-wrapper">
                                                        <a href="#" class="btn btn-upload" id="id290"><i class="ficon ficon-upload"></i> Upload an attachment <span class="max-file-size">(<span class="size">5M</span> max)</span></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    </div>
                </div>
            </section>
            <section id="application-interests" class="application-interests application-edit-section clearfix">
                <header class="profile-panel-title"><h2>My Interests</h2></header>
                <div class="control-group">
                    <div class="control-group input-interests">
                        <label class="control-label">I'm interested in</label>
                        <div class="controls">
                            <select class="input-interests input-block-level" name="">
                                <option value="" disabled selected>Choose interests</option>
                                <option value="0">Interest1</option>
                                <option value="1">Interest1</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-group input-professional-level">
                        <label class="control-label">My professional level is</label>
                        <div class="controls">
                            <select class="input-professional-level input-block-level" name="">
                                <option value="" disabled selected>Choose your level</option>
                                <option value="0">Level1</option>
                                <option value="1">Level2</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="control-group input-relocate">
                        <label class="control-label">I’m willing to work at/relocate to</label>
                        <div class="controls">
                            <select class="input-relocate input-block-level" name="">
                                <option value="" disabled selected>Choose a location</option>
                                <option value="0">Location1</option>
                                <option value="1">Location2</option>
                            </select>
                        </div>
                    </div>
                </div>
            </section>
            <section id="application-privacy" class="application-privacy application-edit-section clearfix">
                <header class="profile-panel-title"><h2>Privacy Settings</h2></header>
                <div class="control-group">
                    <label class="checkbox" id="" for="privacy"><input type="checkbox" name="" id="privacy"> Show my contact details to companies
                    <span>(by default your contact details are private, you can change this later).</span></label>
                </div>
                <div class="control-group">
                    <div class="control-group input-exclude-company">
                        <label class="control-label">Exclude the following companies from seeing my profile:</label>
                        <div class="controls">
                            <input class="input-block-level" type="text" value="" name="" placeholder="Enter company name" />
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <label class="checkbox" id="" for="policy"><input type="checkbox" name="" id="policy"> I have read the <a href="">data security policy</a> and accept it</label>
                </div>
            </section>
            <!-- <button class="btn btn-preview btn-turquoise float-right">Preview</button>
            Temporary button below for demo flow-->
            <a class="btn btn-preview btn-turquoise float-right" href="application-profile-preview.php">Preview</a>
        </form>



    <!-- ************************************************************************

     END FORM

     ***************************************************************************** -->


    </div>
</div>
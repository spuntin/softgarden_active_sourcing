
    <footer>
        <div class="grid">
           <div class="col-1-2">
                <ul>
                    <li><a href="http://www.softgarden.io/company/imprint/">Imprint</a></li>
                    <li><a href="http://www.softgarden.io/company/privacy/">Privacy</a></li>
                </ul>
           </div>
           <div class="col-1-2 right">
                Powered by <a href="http://www.softgarden.io/">softgarden</a>
           </div>
        </div>
    </footer>

    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" ></script>
    <script src="js/jquery.placeholder.min.js"></script>
    <script src="js/chosen.jquery.js" type="text/javascript"></script>
    <script type="text/javascript">

        // Form selectors for jquery plugin Chosen
        var config = {
            '.chosen-select'           : {},
            '.chosen-select-deselect'  : {allow_single_deselect:true},
            '.chosen-select-no-single' : {disable_search_threshold:10},
            '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
            '.chosen-select-width'     : {width:"95%"}
        }
        for (var selector in config) {
            $(selector).chosen(config[selector]);
        }

        // Menu toggle
        $('.ficon-down-open').toggleClass('ficon-up-open');
        $('.nav__dropdown').click(function(e) {
            e.preventDefault();
            $(this).next('ul').slideToggle("slow");
            $(this).find('.ficon-down-open').toggleClass('ficon-up-open');
        });


       

    </script>
    
    <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
    <script>
        function initialize() {
            var map_canvas = document.getElementById('map_canvas');
            var myLatlng = new google.maps.LatLng(52.520007, 13.404954);
            var map_options = {
              center: myLatlng,
              zoom: 8,
              mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(map_canvas, map_options)
            var image = {
                url: 'img/map-marker.png',
                // This marker is 39 x 39 px.
                size: new google.maps.Size(39, 39),
                // The origin for this image is 0,0.
                origin: new google.maps.Point(0,0),
                // The anchor for this image.
                anchor: new google.maps.Point(0, 39)
            };
            var marker = new google.maps.Marker({
                  position: myLatlng,
                  map: map,
                  icon: image
              });
        }
        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</body>

</html>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>softgarden Network</title>

    <link rel="stylesheet" type="text/css" href="css/normalize.css">
    <link rel="stylesheet" type="text/css" href="css/chosen.css" /> <!--  for the 'Chosen' jQuery plugin - It allows multiple select tags on the Job Search page -->
    <link rel="stylesheet" type="text/css" href="css/style.css" media="screen, handheld" />
    <link rel="stylesheet" type="text/css" href="css/enhanced.css" media="(min-width: 769px)" />

    
    <!--[if (lt IE 9)&(!IEMobile)]>
        <link rel="stylesheet" type="text/css" href="css/enhanced.css" />
    <![endif]-->
    <link rel="stylesheet" type="text/css" href="css/fontello.css" />
    <link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Dosis:200,500|Open+Sans:300,400italic,400,600,600italic" />

    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->
</head>

<body>

    <nav class="page__nav">
        <div class="nav__lead clearfix">
            <div class="grid">
                <div class="col-1-1">
                    <div class="nav__content">
                        <ul>
                            <li><a href="http://softgarden.io" class="color-white"><i class="ficon ficon-briefcase"></i> I'm an Employer</a></li>
                            <li><a href="https://softgarden.softgarden-cloud.com/"><i class="ficon ficon-pin"></i> Jobs</a></li>
                            <li class="clearfix">
                                <div class="nav__flags nav__flags--de">
                                    <a href=""><img src="img/de.png" alt="de" width="18" height="12"></a>
                                </div>
                                <div class="nav__flags nav__flags--en">
                                    <img src="img/en.png" height="12" alt="en" width="18">
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="nav__main clearfix">
            <div class="grid">
                <div class="col-1-1">
                    <a class="nav__logo" href="/"><img src="img/logo-softgarden.png" width="195" height="29" alt="Softgarden"></a>
                    <div class="nav__content">

                        <!-- Nav not logged in -->
                        <!-- <img src="img/softgarden-for-job-seekers.png" width="162" height="38" alt="for job seekers" class="nav__subtitle">
                        <button class="btn-login btn-turquoise float-right">Sign In</button>-->

                        <!-- Nav logged in -->
                        <div class="nav__menu-button nav__dropdown"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div>
                        <ul class="nav__main-menu clearfix">
                            <li><a href="applicant-home.php">Home</a></li>
                            <li><a href="applicant-jobs.php">Jobs</a></li>
                            <li class="nav__candidate">
                                <a href="#" class="nav__dropdown"><img src="img/candidate.png" width="35" height="35" alt="Candidate Name"><span class="nav__candidate-name">You</span><i class="ficon ficon-down-open ficon-up-open"></i></a>
                                <ul>
                                    <li><a href="applicant-profile.php">Your Profile</a></li>
                                    <li><a href="applicant-settings.php">Settings</a></li>
                                    <li><a href="applicant-applications.php">Applications</a></li>
                                    <li><a href="#">Sign Out</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>
    </nav>
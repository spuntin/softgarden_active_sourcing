<?php include 'header.php'; ?>
<div class="page__application-wrapper">
    <header class="page__header page__header--personal-landing section section-lightblue bottom-pad0">
        <div class="grid central">
            <div class="col-1-1">
                <h1 class="header-large">Hi [First name], it's <span class="color-pink">time to relax</span> and get invited to your <span class="color-pink">dream job</span>.</h1>
                <p class="p-large">You applied as <strong>[position]</strong> at <strong>[company name]</strong> but this time it didn’t work out. No worries. You can now <strong>activate your profile at softgarden</strong> and show your resume to other companies that might be interested in you.</p> 
                <img src="img/get-invited-to-your-dream-job.png" alt="Get invited to your dream job" width="565" height="226" class="top-marg30">
            </div>
        </div>
    </header>
    <div class="page__body page__body--personal-landing">
        <div class="page__content page__content--personal-companies section section-grey">
            <div class="grid">
                <div class="col-1-1 central">
                    <section class="personal__companies">
                        <header class="section__header">
                            <h2 class="section__title color-dark-blue header-medium slim bottom-marg40">Your next career can start at one of the following companies that are looking for individuals in <strong>[job category]</strong>, and are <strong>[career level]</strong> near <strong>[location]</strong>.</h2>
                        </header>
                        <div class="section__body">
                            <div class="grid">
                                <div class="col-1-6">
                                    <a href="https://www.rebuy.de/" target="_blank"><img src="img/logo-rebuy.png" alt="rebuy.de" width="86" height="28"></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="https://www.brainlab.com/" target="_blank"><img src="img/logo-brainlab.png" alt="BRAINLAB.de" width="99" height="14" ></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="http://misterspex.de/" target="_blank"><img src="img/logo-mister-spex.png" alt="MISTER SPEX" width="112" height="22" ></a>
                                </div>
                                <div class="col-1-6 companies__sanofi">
                                    <a href="http://www.sanofi.com/" target="_blank"><img src="img/logo-sanofi.png" alt="SANOFI" width="68" height="54" ></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="https://www.vitra.com/" target="_blank"><img src="img/logo-vitra.png" alt="vitra." width="68" height="23" ></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="http://www.fresenius.com/" target="_blank"><img src="img/logo-fresenius.png" alt="FRESENIUS" width="95" height="14" ></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
        <div class="page__content page__content--personal-jobs section">
            <div class="grid">
                <div class="col-1-1">
                    <section class="personal__application">
                        <header class="section__header">
                            <h2 class="section__title header-large central">Update my Profile</h2>
                        </header>
                        <?php include 'application-form.php'; ?>
                     </section>
                 </div>
             </div>
         </div>
    </div>
</div>
<?php include 'footer.php'; ?>
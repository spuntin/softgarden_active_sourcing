<?php include 'header.php'; ?>

    <header class="page__header page__header--applications section-turquoise">
        <div class="grid">
            <div class="col-1-1">
                <h1 class="page__title">Applications</h1>
            </div>
        </div>
    </header>
    <div class="page__body page__body--applications section">
        <div class="grid">
            <div class="col-1-1">
                <section class="applications__list">
                    <table class="application-list__table bottom-marg30">
                    <thead>
                        <tr>
                            <th><a href="">Company</a></th>
                            <th><a href="">Job</a></th>
                            <th><a href="">Location</a></th>
                            <th><a href="">Date <i class="ficon ficon-down-open"></i></a></th>
                            <th><a href="">Status</a></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><img src="img/logo-jobs-available-vitra.png" alt="Vitra" width="74" height="25"></td>
                            <td>Senior Marketing Manager m/...</td>
                            <td>Berlin, DE</td>
                            <td>04.09.14</td>
                            <td>Transferred to a new job</td>
                            <td><a href="">PDF</a></td>
                            <td><button class="btn btn-delete btn-grey float-left">Delete</button></td>
                        </tr>
                        <tr>
                            <td><img src="img/logo-jobs-available-brainlab.png" alt="BRAINLAB" width="75" height="10"></td>
                            <td>Growth Marketing Manager</td>
                            <td>Mönchengladb...</td>
                            <td>04.09.14</td>
                            <td>Open</td>
                            <td><a href="">PDF</a></td>
                            <td><button class="btn btn-delete btn-grey float-left">Withdraw</button><button class="btn btn-update btn-turquoise float-left">Update</button></td>
                        </tr>
                    </tbody>
                </table>
                 </section>
             </div>
         </div>
     </div>

<?php include 'footer.php'; ?>
<?php include 'header.php'; ?>

    <header class="page__header page__header--preview section-turquoise">
        <div class="grid">
            <div class="col-1-1">
                <h1 class="page__title">Your Profile - Preview before You Submit</h1>
            </div>
        </div>
    </header>

    <?php include 'profile-content.php'; ?>

<?php include 'footer.php'; ?>
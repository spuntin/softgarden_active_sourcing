<?php include 'header.php'; ?>

    <header class="page__header page__header--jobs section-turquoise">
        <div class="grid">
            <div class="col-1-1">
                <h1 class="page__title">Jobs</h1>
            </div>
        </div>
    </header>

    <div class="page__body page__body--jobs">
        <div class="page__search section-turquoise">
            <div class="grid">
                <div class="col-1-1">
                    <form class="job-search__form">
                        <fieldset>

                            <div class="col-1-5">
                                <label class="control-label" for="keyword">Keyword</label>
                                <div class="controls">
                                    <input id="keyword" name="keyword" type="text" placeholder="Search">
                                </div>
                            </div>

                            <div class="col-1-5">
                                <label class="control-label" for="location">Location</label>
                                <div class="controls">
                                <select data-placeholder="Enter Location" multiple class="chosen-select" id="location" name="location">
                                        <option value=""></option>
                                        <option>Location1</option>
                                        <option>Location2</option>
                                        <option>Location3</option>
                                        <option>Location4</option>
                                        <option>Location5</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-1-5">
                                <label class="control-label" for="distance">Distance</label>
                                <div class="controls">
                                    <select class="chosen-select-no-single">
                                        <option value="Within 10km">Within 10km</option>
                                        <option value="Within 25km" selected>Within 25km</option>
                                        <option value="Within 50km">Within 50km</option>
                                        <option value="Within 75km">Within 75km</option>
                                        <option value="Within 100km">Within 100km</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-1-5">
                                <label class="control-label" for="category">Job Category</label>
                                <div class="controls">
                                    <select data-placeholder="Select Categories" multiple class="chosen-select" id="category" name="category">
                                        <option value=""></option>
                                        <option>Engineering</option>
                                        <option>Finance</option>
                                        <option>IT</option>
                                        <option>Management</option>
                                        <option>Marketing</option>
                                        <option>Media</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-1-5 right-pad0">
                                <label class="control-label" for="level">Career Level</label>
                                <div class="controls">
                                    <select data-placeholder="Select Levels" multiple class="chosen-select" id="levek" name="level">
                                        <option value=""></option>
                                        <option>Entry</option>
                                        <option>Mid</option>
                                        <option>Senior</option>
                                    </select>
                                </div>
                            </div>

                            <div class="control-group job-search__submit bottom-pad0">
                                <div class="controls">
                                    <button name="go" class="btn btn-dark-blue">Los</button>
                                </div>
                            </div>

                        </fieldset>

                    </form>
                </div>
            </div>
        </div>

        <div class="page__map">
            <div class="job-search__map bottom-marg20">
                <div id="map_canvas"></div>
            </div>
        </div>

        <div class="page__content section">
            <div class="grid">
                <div class="col-1-1">
                    <section class="candidate-job-search__job-proposals">                        
                        <div class="section__body clearfix">
                            <div class="col-1-2 bottom-marg30">
                                Your search returned <strong>15 jobs</strong>
                            </div>
                            <div class="col-1-2 bottom-marg30 right ">
                                <a href="" class="color-pink">Subscribe to Jobs</a>
                            </div>
                            
                            <?php include 'job-list.php'; ?>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>
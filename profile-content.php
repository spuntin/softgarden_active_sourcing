<div class="page__body page__body--preview section">
    <div class="page__content">
        <div class="grid">
            <div class="col-2-5">
                <section class="profile__details">
                    <header class="section__header">
                        <h2 class="section__title">Personal Details</h2>
                    </header>
                    <div class="section__body">
                        <div class="grid grey-box">
                            <div class="col-2-5">
                                <img src="img/applicant-profile.jpg" alt="Applicant Name" width="131" height="178">
                            </div>
                            <div class="col-3-5">
                                <h3>Miss Sharon Hermann</h3>
                                <p>
                                    s.hermann@mailme.de<br />
                                    01523654178
                                </p>
                                <p>
                                    Leyendeckerstrasse 34 50827 Koln
                                </p>    
                            </div>
                        </div>
                    </div>
                </section>
            </div>
            <div class="col-3-5">
                <section>
                    <header class="section__header">
                        <h2 class="section__title">Experience</h2>
                        <a href="applicant-profile-edit.php" class="profile__edit"><i class="ficon ficon-pencil"></i> Edit Profile</a>
                    </header>
                    <div class="section__body">
                        <h3>Human Resources Manager</h3>
                        <p>
                            at WEST REHA. West Creek, Mississippi
                            <span>July 2011 – August 2013</span>
                        </p>
                        <p>
                            Directed and managed staffing for 22 facilities. Recruited, hired, and trained staff. Supervised five staffing coordinators and handled all HR management functions.
                        </p>
                        <ul>
                            <li>Successfully improved company profits through effective management of human resources.</li>
                            <li>Conducted interviews, screened applicants, and provided new hire orientations.</li>
                            <li>Authored employee handbooks for three companies.30</li>
                            <li>Implemented custom HRIS system to coordinate resource alloction and staff scheduling.</li>
                            <li>Developed and implemented policies, procedures, and initiatives that improved customer relations and dramatically reduced staff turnover.</li>
                        </ul>
                        <p>
                            Directed and managed staffing for 22 facilities. Recruited, hired, and trained staff. Supervised five staffing coordinators and handled all HR management functions.
                        </p>
                        <h3>Partnership Manager</h3>
                        <p>
                            at ADVO STAFFING. West Hanson, Mississippi
                            <span>July 2010 – Juni 2011</span>
                        </p>
                        <p>
                            Managed operations and supervised staff at three locations. Hired, trained, supervised, and evaluated 24 on-site coordinators. Functioned as primary liaison between client companies and upper management. Assisted with contract negotiation and recruiting.
                        </p>
                    </div>
                </section>
                <section>
                    <header class="section__header">
                        <h2 class="section__title">Education</h2>
                    </header>
                    <div class="section__body">
                        <h3>B.Sc Human Resource Management</h3>
                        <p>
                            at the University of Phoenix<br />
                            Grade: 2.5
                            <span>2009 - 2011</span>
                        </p>
                    </div>
                </section>
                <section>
                    <header class="section__header">
                        <h2 class="section__title">Language</h2>
                    </header>
                    <div class="section__body">
                        <p>
                            English Fluent<br />
                            German intermediate 
                        </p>
                    </div>
                </section>
                <section>
                    <header class="section__header">
                        <h2 class="section__title">My Interests</h2>
                    </header>
                    <div class="section__body">
                        <p>
                            I’m interested in:<br />
                            <strong>Design, Marketing, Development</strong><br />
                            My professional level is:<br />
                            <strong>Senior</strong><br />
                            I’m willing to work at/relocate to:<br />
                            <strong>Germany, Poland, Hungary</strong><br />
                        </p>
                    </div>
                </section>
                <!-- <button class="btn-grey float-left">Back</button>
                <button class="btn-turquoise float-right">Submit</button>
                Temporary button below for demo flow-->
                <a href="application-landing-email.php" class="btn-grey float-left">Back</a>
                <a href="applicant-home.php" class="btn-turquoise float-right">Submit</a>
           </div>
        </div>
    </div>
</div>
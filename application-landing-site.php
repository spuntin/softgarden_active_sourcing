<?php include 'header.php'; ?>

<div class="page__application-wrapper">

    <header class="page__header page__header--landing section section-lightblue bottom-pad0">
        <div class="grid central">
            <div class="col-1-1">
                <h1 class="header-large">It's time to get invited to your <span class="color-turquoise">dream job</span>.</h1>
                <p class="p-large">Join softgarden to get invited to great jobs. <strong>It’s secure, private & easy.</strong></p>
                <form class="form__invite clearfix">

                    <div class="form__control form__control--email float-left">
                        <input id="email" name="email" type="text" placeholder="Enter your email address">
                    </div>

                    <div class="form__control form__control--create-profile float-right">
                        <button id="create-profile" name="create-profile" class="btn btn-create-profile btn-turquoise">Create Your Profile</button>
                    </div>

                </form>

                <img src="img/create-active-sourcing-profile.png" alt="Create your softgarden Active Sourcing profile" width="717" height="289">
            </div>
        </div>
    </header>

    <div class="page__body page__body--landing">
        <div class="page__content page__content--companies section section-grey">
            <div class="grid">
                <div class="col-1-1 central">
                    <section class="application__companies">
                        <header class="section__header">
                            <h2 class="section__title header-large">Start your next career at one of these companies & others by subscribing</h2>
                        </header>
                        <div class="section__body">
                            <div class="grid">
                                <div class="col-1-6">
                                    <a href="https://www.rebuy.de/" target="_blank"><img src="img/logo-rebuy.png" alt="rebuy.de" width="86" height="28"></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="https://www.brainlab.com/" target="_blank"><img src="img/logo-brainlab.png" alt="BRAINLAB.de" width="99" height="14" ></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="http://misterspex.de/" target="_blank"><img src="img/logo-mister-spex.png" alt="MISTER SPEX" width="112" height="22" ></a>
                                </div>
                                <div class="col-1-6 companies__sanofi">
                                    <a href="http://www.sanofi.com/" target="_blank"><img src="img/logo-sanofi.png" alt="SANOFI" width="68" height="54" ></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="https://www.vitra.com/" target="_blank"><img src="img/logo-vitra.png" alt="vitra." width="68" height="23" ></a>
                                </div>
                                <div class="col-1-6">
                                    <a href="http://www.fresenius.com/" target="_blank"><img src="img/logo-fresenius.png" alt="FRESENIUS" width="95" height="14" ></a>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>

        <div class="page__content page__content--jobs section">
            <div class="grid">
                <div class="col-1-1 central">
                    <section class="application__jobs">
                        <header class="section__header">
                            <h2 class="section__title header-large">It’s never been <span class="color-turquoise">easier</span> to find the perfect job</h2>
                        </header>
                        <div class="section__body">
                            <div class="grid central">
                                <div class="col-1-3">
                                    <img src="img/receive-invitations-to-great-jobs.png" alt="Receive invitations to great jobs" width="200" height="200" class="bottom-marg30">
                                    <h3 class="left bottom-marg20">Receive invitations to great jobs</h3>
                                    <p class="p-large left">Our cloud software platform is used by hundreds of companies looking at finding great talent like yourself. Once you subscribe you can either actively or passively search for the right opportunity/challenge that you have been looking for in one secure, private, & easy way.</p>
                                </div>
                                <div class="col-1-3">
                                    <img src="img/track-all-your-applications.png" alt="Receive invitations to great jobs" width="200" height="200" class="bottom-marg30">
                                    <h3 class="left bottom-marg20">Track all of your applications</h3>
                                    <p class="p-large left">Be able to quickly & easily see to where & when you have applied those next great opportunities. If you would like to see the job description, then no problem. You can click on each individual application & see the details of the job that you applied to.</p>
                                </div>
                                <div class="col-1-3">
                                    <img src="img/compare-your-job-offers.png" alt="Receive invitations to great jobs" width="200" height="200" class="bottom-marg30">
                                    <h3 class="left bottom-marg20">Compare your job offers</h3>
                                    <p class="p-large left">Through one simple page you can easily access job descriptions & offers of multiple companies that are looking at hiring you. Once you have made a decision, quickly & easily manage your application letting employers know your decision. There is no need to worry about the response getting lost somewhere.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

</div>

<?php include 'footer.php'; ?>
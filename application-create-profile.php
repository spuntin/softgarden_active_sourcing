<?php include 'header.php'; ?>
<div class="page__application-wrapper">
    <div class="page__body page__body--create-profile section">
        <div class="grid">
            <div class="col-1-1">
                <section class="profile__create">
                    <header class="section__header">
                        <h2 class="section__title header-large central">Create my Profile</h2>
                    </header>
                    <?php include 'application-form.php'; ?>
                 </section>
             </div>
         </div>
     </div>
</div>
<?php include 'footer.php'; ?>
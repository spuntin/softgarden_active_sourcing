<?php include 'header.php'; ?>

    <header class="page__header page__header--settings section-turquoise">
        <div class="grid">
            <div class="col-1-1">
                <h1 class="page__title">Settings</h1>
            </div>
        </div>
    </header>
    <div class="page__body page__body--settings section">
        <div class="grid">
            <div class="col-1-1">
                <section class="profile__edit">
                
                    <div class="section__body section__body--profile-form">
                        <div class="grid">
                            <form class="settings__form" id="" method="post" action="" enctype="multipart/form-data" accept-charset="UTF-8">
                                <section class="settings__password clearfix">
                                    <header class="profile-panel-title"><h2>Change Password</h2></header>
                                    <div class="control-group">
                                        <div class="control-group input-old-password">
                                            <label class="control-label" for="old-password">Old Password</label>
                                            <div class="controls">
                                                <input id="old-password" name="old-password" type="text">
                                            </div>
                                        </div>
                                        <div class="control-group input-new-password">
                                            <label class="control-label" for="new-password">New Password</label>
                                            <div class="controls">
                                                <input id="new-password" name="new-password" type="text">
                                            </div>
                                        </div>
                                        <div class="control-group input-repeat-password">
                                            <label class="control-label" for="repeat-password">Repeat New Password</label>
                                            <div class="controls">
                                                <input id="repeat-password" name="repeat-password" type="text">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-save btn-turquoise float-right">Save</button>
                                </section>
                                <section class="settings__privacy application-privacy application-edit-section clearfix">
                                    <header class="profile-panel-title"><h2>Privacy Settings</h2></header>
                                    <div class="control-group">
                                        <label class="checkbox" id="" for="privacy"><input type="checkbox" name="" id="privacy"> Show my contact details to companies
                                        <span>(by default your contact details are private, you can change this later).</span></label>
                                    </div>
                                    <div class="control-group">
                                        <div class="control-group input-exclude-company">
                                            <label class="control-label" for="exclude-companies">Exclude the following companies from seeing my profile:</label>
                                            <div class="controls">
                                                <input class="input-block-level" type="text" value="" id="exclude-companies" name="exclude-companies" placeholder="Enter company name" />
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-save btn-turquoise float-right">Save</button>
                                </section>
                                <section class="settings__delete application-edit-section clearfix">
                                    <header class="profile-panel-title"><h2>Delete Account</h2></header>
                                    <p>Click “Delete Account” to permanently remove your account from the system and, where appropriate, to withdraw dispatched applications.</p>
                                    <button class="btn btn-delete btn-grey float-right">Delete Account</button>
                                </section>
                            </form>
                        </div>
                    </div>

                 </section>
             </div>
         </div>
     </div>

<?php include 'footer.php'; ?>